/*
	Compute OEIS A111441 entries
	See https://oeis.org/A111441

	Copyright (C) 2020-2022  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// https://github.com/kimwalisch/primesieve
// https://bluescarni.github.io/mppp/installation.html
// https://oeis.org/A111441/list
// g++ -O3 just_count.cpp -o just_count -lprimesieve -lmp++ -lgmp -pthread
// ./just_count --continue counts.txt | tee -a counts.txt

#include <stdio.h>
#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <vector>
#include <algorithm>
#include <string>
#include <regex>
#include <fstream>
#include <filesystem>
#include <stdlib.h>
#include <math.h>
#include <primesieve.hpp>
#include <mp++/integer.hpp>

size_t NUM_WORKERS = 4; // how many worker threads to use

#define IMAX  200000000000000000
#define CYCLE         6000000000 // how often print checkpoint


using namespace std;

typedef unsigned __int128 __uint128;
const __uint128 UINT128_MAX = ~((__uint128)0);
typedef mppp::integer<3> sum_t;

// id: index of block from 0
// primes counted in range [p_{first_i}, p_{last_i}]
// length: last_i - first_i + 1
// partial_sum: sum within block
// sum: accumulative sum, including all previous blocks
// processed: length and partial_sum computed, but not necessarily sum and indices
typedef struct  {
	size_t id;
	uint64_t first_i;
	uint64_t last_i;
	uint64_t length;
	__uint128 first_prime;
	__uint128 last_prime;
	sum_t partial_sum;
	sum_t sum;
	bool processed;
} block_t;

static_assert(sizeof(unsigned long long) == sizeof(uint64_t));



mutex vector_mtx;
vector<block_t> blocks;
condition_variable cv;


std::chrono::_V2::system_clock::time_point start;

void print_uint128(__uint128 n){
	if(n == 0){
		return;
	}

	print_uint128(n/10);
	putchar(n%10+'0');
}

void print_uint128(__uint128 n, fstream &file){
	if(n == 0){
		return;
	}

	print_uint128(n/10, file);
	file << char(char(n%10)+char('0'));
}

__uint128 stoulll(const std::string & number){
	__uint128 res = 0;

	__uint128 multiplier = 1;
	for(int pos = number.size() - 1; pos>=0; pos--){
		char digit = number[pos];
		digit -= '0';

		res += digit*multiplier;
		multiplier *= 10;
	}

	return res;
}

double seconds(){
	auto now = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = now - start;
	return diff.count();
}

void worker(size_t);

size_t first_block_id = 0;
size_t last_block_id = 0;
std::atomic<size_t> block_id = 0;
void init_worker(size_t);

mutex block_boundary_db_file_mtx;
fstream block_boundary_db_file;
bool load_precomputed_boundaries = false;
size_t first_block_not_yet_written_to_block_boundary_db = 0;
void write_to_block_boundary_db();


// thread to generate vector of blocks
void init(){
	primesieve::iterator it;

	block_t block = {
		0,
		0, 
		0,
		0,
		0,
		0,
		sum_t(0),
		sum_t(0),
		false
	};

	blocks.resize(IMAX/CYCLE + 1, block);

	if(last_block_id > 0){
		last_block_id = min(last_block_id, blocks.size() - 2);
	}else{
		last_block_id = blocks.size() - 2;
	}

	if(first_block_id>0){
		block_id = first_block_id-1;
	}else{
		block_id = 0;
	}
	first_block_not_yet_written_to_block_boundary_db = first_block_id;

	if(load_precomputed_boundaries){
		block_boundary_db_file.seekg(0, block_boundary_db_file.beg);

		std::string line{};
		std::regex rgx("^IMAX=(\\d+), CYCLE=(\\d+)$");
		getline(block_boundary_db_file, line);

		std::smatch match;
		bool good_cycles = false;
		if(std::regex_search(line, match, rgx)){
			good_cycles = std::stoull(match[1]) == IMAX && std::stoull(match[2]) == CYCLE;
		}

		if(!good_cycles){
			std::cerr << "Boundaries DB file cycle mismatch\n";
			exit(1);
		}

		//consume empy line and check CSV header
		if(!getline(block_boundary_db_file, line) || !getline(block_boundary_db_file, line) || line != "id,first_prime,last_prime"){
			std::cerr << "Wrong boundary DB format\n";
			exit(1);
		}

		block_id = 0;

		std::regex line_rgx("^(\\d+),(\\d+),(\\d+)$");
		while(getline(block_boundary_db_file, line)){
			if(!std::regex_search(line, match, line_rgx)){
				break;
			}

			const size_t read_id = std::stoull(match[1]);
			const __uint128 read_first_prime = stoulll(match[2]);
			const __uint128 read_last_prime = stoulll(match[3]);

			if(read_id != block_id){
				std::cerr << "Wrong boundary DB data\n";
				exit(1);
			}

			blocks[block_id].first_prime = read_first_prime;
			blocks[block_id].last_prime = read_last_prime;
			blocks[block_id].id = block_id;


			if(read_first_prime > read_last_prime || (block_id>0 && blocks[block_id-1].last_prime >= read_first_prime)){
				std::cerr << "Wrong boundary DB data\n";
				exit(1);
			}

			block_id++;
		}

		first_block_not_yet_written_to_block_boundary_db = block_id;
		if(block_id>0){
			block_id--;
		}

	}

	printf("[init] [%.1fs] block vector initialised\n", seconds());
	fflush(stdout);

	blocks[0].first_i = 1;
	blocks[0].first_prime = 2;

	thread th_workers[NUM_WORKERS+1];
	for(size_t i=0; i<NUM_WORKERS+1; i++){
		th_workers[i] = thread(init_worker,i);
	}
	for (auto& th : th_workers) th.join();

	blocks.resize(blocks.size() - 1);

	printf("[init] [%.1fs] init completed.\n", seconds());
	
	write_to_block_boundary_db();

	fflush(stdout);
}

// calculate block boundaries
void init_worker(size_t wid){
	primesieve::iterator it;
	uint64_t i;
	
	const size_t blocks_size = blocks.size() - 1;
	size_t id;
	while((id=block_id++) <= last_block_id){
		i = (id+1)*CYCLE;
		it.skipto((double)i * log(i));
		blocks[id].last_prime = it.next_prime();

		blocks[id+1].id = id+1;
		blocks[id+1].first_prime = it.next_prime();

		if(id % 1000 == 0){
			printf("[init] [%.1fs] %d/%d\n", seconds(), id, blocks_size);
			fflush(stdout);
			write_to_block_boundary_db();
		}
	}
}

// writes newly calculated block boundaries to db
void write_to_block_boundary_db(){
	const std::lock_guard<std::mutex> lock(block_boundary_db_file_mtx);

	if(first_block_not_yet_written_to_block_boundary_db == 0){
		block_boundary_db_file << "IMAX=" << IMAX << ", CYCLE=" << CYCLE << "\n\n";
		block_boundary_db_file << "id,first_prime,last_prime\n";
	}

	for(size_t i = first_block_not_yet_written_to_block_boundary_db; i<=last_block_id; i++){
		const auto& block = blocks[i];
		if(block.last_prime == 0 || block.first_prime == 0){
			break;
		}
		block_boundary_db_file << block.id << ",";
		print_uint128(block.first_prime, block_boundary_db_file);
		block_boundary_db_file << ",";
		print_uint128(block.last_prime, block_boundary_db_file);
		block_boundary_db_file << "\n";

		first_block_not_yet_written_to_block_boundary_db = i+1;
	}
}

// process items in the block vector
void worker(size_t wid){
	printf("[worker%d] [%.1fs] worker started\n", wid, seconds());
	fflush(stdout);
	primesieve::iterator it;
	__uint128 prime;

	uint64_t length;
	__uint128 partial_sum128;
	__uint128 square;
	sum_t partial_sum;

	const size_t blocks_size = blocks.size();
	size_t id;
	while((id=block_id++) < blocks_size){
		if(blocks[id].processed){
			// loaded from checkpoint, skip
			continue;
		}
		printf("[worker%d] [%.1fs] started processing block #%d\n", wid, seconds(), id);
		fflush(stdout);
		block_t & block = blocks[id];

		it.skipto(block.first_prime - 1);
		
		length = 0;
		partial_sum128 = 0;
		partial_sum = 0;
		while((prime = it.next_prime()) <= block.last_prime){
			length++;
			square = prime*prime;

			//avoid overflow
			if(partial_sum128 > UINT128_MAX - square){
				partial_sum += partial_sum128;
				partial_sum128 = 0;
			}
			partial_sum128 += square;
		}

		partial_sum += partial_sum128;

		lock_guard<std::mutex> lg(vector_mtx); 
		block.length = length;
		block.partial_sum = partial_sum;
		block.processed = true;
		printf("[worker%d] [%.1fs] finished processing block #%d\n", wid, seconds(), id);
		fflush(stdout);
		cv.notify_one();
	}
}

// thread summing all partial sums
// and summing indices
void summer(size_t start_id){
	size_t id;
	uint64_t i;
	sum_t sum;

	if(start_id == 0){
		id = 0;
		i = 0;
		sum =  0;
	}else{
		//blocks fully processed until this id
		id = start_id;
		i = blocks[id].last_i;
		sum = blocks[id].sum;
		id++;
	}

	const size_t blocks_size = blocks.size();
	while(id < blocks_size){
		unique_lock<mutex> ul(vector_mtx);
		cv.wait(ul);
		while(id < blocks_size && blocks[id].processed){
			blocks[id].first_i = i+1;
			i += blocks[id].length;
			blocks[id].last_i = i;
			sum += blocks[id].partial_sum;
			blocks[id].sum = sum;

			printf("[summer] [%.1fs] block #%d computed\n", seconds(), id);
			printf("\tid=%d\n", id);
			printf("\tfirst_i=%" PRIu64 "\n", blocks[id].first_i);
			printf("\tlast_i=%" PRIu64 "\n", blocks[id].last_i);
			printf("\tlength=%" PRIu64 "\n", blocks[id].length);
			printf("\tfirst_prime=");
			print_uint128(blocks[id].first_prime);
			printf("\n\tlast_prime=");
			print_uint128(blocks[id].last_prime);
			printf("\n\tpartial_sum="); cout << blocks[id].partial_sum;
			printf("\n\tsum="); cout << blocks[id].sum;
			printf("\n\n");
			fflush(stdout);

			id++;
		}
		ul.unlock();
	}
}

char * cmd_option(char ** begin, char ** end, const string & option) {
	char ** itr = std::find(begin, end, option);
	if(itr != end && ++itr != end){
		return *itr;
	}
	return 0;
}

bool cmd_option_exists(char** begin, char** end, const string& option){
	return std::find(begin, end, option) != end;
}

int main(int argc, char ** argv){
	start = std::chrono::high_resolution_clock::now();

	if(cmd_option_exists(argv, argv+argc, "--threads")) {
		char * numthreads = cmd_option(argv, argv + argc, "--threads");
		NUM_WORKERS = atoi(numthreads);
	}

	if(cmd_option_exists(argv, argv+argc, "--block-boundary-db")) {
		char * filename = cmd_option(argv, argv + argc, "--block-boundary-db");
		if(std::filesystem::file_size(filename) > 0){
			// there's some pre-computed data
			block_boundary_db_file.open(filename, std::fstream::in | std::fstream::out | std::fstream::app);
			load_precomputed_boundaries = true;
		}else{
			block_boundary_db_file.open(filename, std::fstream::out);
			load_precomputed_boundaries = false;
		}
		if(!block_boundary_db_file.is_open()){
			cerr << "Failed to open file " << filename << endl;
			return -1;
		}
	}else{
		cerr << "Please supply --block-boundary-db\n";
		return -1;
	}

	bool exit_after_boundary_db_calculated = false;

	if(cmd_option_exists(argv, argv+argc, "--first-block-id")) {
		char * bi = cmd_option(argv, argv + argc, "--first-block-id");
		first_block_id = atoi(bi);
		exit_after_boundary_db_calculated = true;
	}

	if(cmd_option_exists(argv, argv+argc, "--last-block-id")) {
		char * bi = cmd_option(argv, argv + argc, "--last-block-id");
		last_block_id = atoi(bi);
		exit_after_boundary_db_calculated = true;
	}

	init();

	block_boundary_db_file.close();

	exit_after_boundary_db_calculated = exit_after_boundary_db_calculated || cmd_option_exists(argv, argv+argc, "--exit-after-boundary-db-calculated");

	if(exit_after_boundary_db_calculated){
		return 0;
	}

	size_t id = 0;
	// load previously computed values
	if(cmd_option_exists(argv, argv+argc, "--continue")) {
		char * filename = cmd_option(argv, argv + argc, "--continue");
		ifstream file(filename);
		if(!file.is_open()){
			cerr << "Failed to open file " << filename << endl;
			return -1;
		}
		std::string line;
		std::regex num_rgx("^\t.+=(\\d+)$");
		std::regex id_rgx("^\tid=(\\d+)$");



		while(getline(file, line)){
			std::smatch match;
			if(std::regex_search(line, match, id_rgx)){
				id = std::stoull(match[1]);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].first_i = std::stoull(match[1]);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].last_i = std::stoull(match[1]);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].length = std::stoull(match[1]);
				getline(file, line); getline(file, line);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].partial_sum = match.str(1);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].sum = match.str(1);
				blocks[id].processed = true;

			}
		}
		file.close();
		printf("[main] [%.1fs] pre-computed blocks loaded. Continuing from #%d\n", seconds(), id+1);
		fflush(stdout);
	}

	block_id = 0;
	
	thread th_workers[NUM_WORKERS];

	for(size_t i=0; i<NUM_WORKERS; i++){
		th_workers[i] = thread(worker,i);
	}

	thread th_summer(summer, id);

	for (auto& th : th_workers) th.join();
	th_summer.join();

	return 0;
}
