/*
	Compute OEIS A111441 entries
	See https://oeis.org/A111441

	Copyright (C) 2020-2022  Balázs Dura-Kovács

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// this assumes that boundary db has been pre-calculated

#include <stdio.h>
#include <iostream>
#include <chrono>
#include <vector>
#include <algorithm>
#include <atomic>
#include <csignal>
#include <string>
#include <regex>
#include <fstream>
#include <filesystem>
#include <stdlib.h>
#include <math.h>
#include <primesieve.hpp>
#include <mp++/integer.hpp>
#include <mpi.h>



#define IMAX  200000000000000000
#define CYCLE         6000000000 // how often print checkpoint

using namespace std;

int my_rank;

volatile std::atomic<bool> sigusr1_received = false;
void sigusr1_handler(int s) {
    sigusr1_received = true;
    std::cout << "[rank" << my_rank << "] signal recieved: " << s << std::endl;
}

typedef unsigned __int128 __uint128;
const __uint128 UINT128_MAX = ~((__uint128)0);
typedef mppp::integer<3> sum_t;

// id: index of block from 0
// primes counted in range [p_{first_i}, p_{last_i}]
// length: last_i - first_i + 1
// partial_sum: sum within block
// sum: accumulative sum, including all previous blocks
// processed: length and partial_sum computed, but not necessarily sum and indices
typedef struct  {
	size_t id;
	uint64_t first_i;
	uint64_t last_i;
	uint64_t length;
	__uint128 first_prime;
	__uint128 last_prime;
	sum_t partial_sum;
	sum_t sum;
	bool processed;
} block_t;

static_assert(sizeof(unsigned long long) == sizeof(uint64_t));


vector<block_t> blocks;
fstream block_boundary_db_file;

std::chrono::_V2::system_clock::time_point start;

void print_uint128(__uint128 n){
	if(n == 0){
		return;
	}

	print_uint128(n/10);
	putchar(n%10+'0');
}

void print_uint128(__uint128 n, fstream &file){
	if(n == 0){
		return;
	}

	print_uint128(n/10, file);
	file << char(char(n%10)+char('0'));
}

__uint128 stoulll(const std::string & number){
	__uint128 res = 0;

	__uint128 multiplier = 1;
	for(int pos = number.size() - 1; pos>=0; pos--){
		char digit = number[pos];
		digit -= '0';

		res += digit*multiplier;
		multiplier *= 10;
	}

	return res;
}

double seconds(){
	auto now = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = now - start;
	return diff.count();
}

void worker();




// load vector of blocks
void init(){


	block_t block = {
		0,
		0, 
		0,
		0,
		0,
		0,
		sum_t(0),
		sum_t(0),
		false
	};

	blocks.resize(IMAX/CYCLE + 1, block);

	printf("[init] [%.1fs] block vector initialised\n", seconds());
	fflush(stdout);



	block_boundary_db_file.seekg(0, block_boundary_db_file.beg);

	std::string line{};
	std::regex rgx("^IMAX=(\\d+), CYCLE=(\\d+)$");
	getline(block_boundary_db_file, line);

	std::smatch match;
	bool good_cycles = false;
	if(std::regex_search(line, match, rgx)){
		good_cycles = std::stoull(match[1]) == IMAX && std::stoull(match[2]) == CYCLE;
	}

	if(!good_cycles){
		std::cerr << "Boundaries DB file cycle mismatch\n";
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	//consume empy line and check CSV header
	if(!getline(block_boundary_db_file, line) || !getline(block_boundary_db_file, line) || line != "id,first_prime,last_prime"){
		std::cerr << "Wrong boundary DB format\n";
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	size_t block_id = 0;

	std::regex line_rgx("^(\\d+),(\\d+),(\\d+)$");
	while(getline(block_boundary_db_file, line)){
		if(!std::regex_search(line, match, line_rgx)){
			break;
		}

		const size_t read_id = std::stoull(match[1]);
		const __uint128 read_first_prime = stoulll(match[2]);
		const __uint128 read_last_prime = stoulll(match[3]);

		if(read_id != block_id){
			std::cerr << "Wrong boundary DB data\n";
			MPI_Abort(MPI_COMM_WORLD, 1);
		}

		blocks[block_id].first_prime = read_first_prime;
		blocks[block_id].last_prime = read_last_prime;
		blocks[block_id].id = block_id;


		if(read_first_prime > read_last_prime || (block_id>0 && blocks[block_id-1].last_prime >= read_first_prime)){
			std::cerr << "Wrong boundary DB data\n";
			MPI_Abort(MPI_COMM_WORLD, 1);
		}

		block_id++;
	}


	blocks[0].first_i = 1;
	blocks[0].first_prime = 2;

	blocks.resize(blocks.size() - 1);
	

	printf("[init] [%.1fs] init completed.\n", seconds());
	fflush(stdout);
}




// process items in the block vector
void worker(){
	primesieve::iterator it;
	__uint128 prime;

	uint64_t length;
	__uint128 partial_sum128;
	__uint128 square;
	sum_t partial_sum;

	size_t id;
	block_t block;
	while(true){
		MPI_Status status;
		MPI_Recv((void*) &block, sizeof(block_t), MPI_BYTE, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		if(status.MPI_TAG == 1){ // exit signal
			return;
		}

		it.skipto(block.first_prime - 1);
		
		length = 0;
		partial_sum128 = 0;
		partial_sum = 0;
		while((prime = it.next_prime()) <= block.last_prime){
			length++;
			square = prime*prime;

			//avoid overflow
			if(partial_sum128 > UINT128_MAX - square){
				partial_sum += partial_sum128;
				partial_sum128 = 0;
			}
			partial_sum128 += square;
		}

		partial_sum += partial_sum128;


		block.length = length;
		block.partial_sum = partial_sum;
		block.processed = true;

		MPI_Send((void *) &block, sizeof(block_t), MPI_BYTE, 0, 0, MPI_COMM_WORLD);
	}
}

// thread summing all partial sums
// and summing indices
void summer(size_t start_id){
	size_t id;
	uint64_t i;
	sum_t sum;

	if(start_id == 0){
		id = 0;
		i = 0;
		sum =  0;
	}else{
		//blocks fully processed until this id
		id = start_id;
		i = blocks[id].last_i;
		sum = blocks[id].sum;
		id++;
	}

	size_t queue_id = id; // first block id to send out for processing

	const size_t blocks_size = blocks.size();
	int num_procs;
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

	// start processing in worker processes
	for(int worker_rank=1; worker_rank < num_procs; worker_rank++){
		if(queue_id < blocks_size){
			MPI_Send((void *) &(blocks[queue_id]), sizeof(block_t), MPI_BYTE, worker_rank, 0, MPI_COMM_WORLD);
			printf("[worker%d] [%.1fs] started processing block #%d\n", worker_rank, seconds(), queue_id);
			queue_id++;
		}else{
			// send signal to finish
			char f = 1;
			MPI_Send(&f, 1, MPI_BYTE, worker_rank, 1, MPI_COMM_WORLD);
			printf("[worker%d] [%.1fs] terminated\n", worker_rank, seconds());
		}
		fflush(stdout);
	}


	while(id < blocks_size && !sigusr1_received){
		MPI_Status status;
		block_t processed_block;
		printf("[debug] [%.1fs] before MPI_Recv, sigusr1_received=%d\n", seconds(), sigusr1_received.load());
		MPI_Recv((void*) &processed_block, sizeof(block_t), MPI_BYTE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
		printf("[debug] [%.1fs] after MPI_Recv, sigusr1_received=%d\n", seconds(), sigusr1_received.load());
		blocks[processed_block.id] = processed_block;
		printf("[worker%d] [%.1fs] finished processing block #%d\n", status.MPI_SOURCE, seconds(), id);
		fflush(stdout);
		// send new block to the same process
		if(queue_id < blocks_size && !sigusr1_received){
			MPI_Send((void *) &(blocks[queue_id]), sizeof(block_t), MPI_BYTE, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
			printf("[worker%d] [%.1fs] started processing block #%d\n", status.MPI_SOURCE, seconds(), queue_id);
			fflush(stdout);
			queue_id++;
		}else{
			// send signal to finish
			char f = 1;
			MPI_Send(&f, 1, MPI_BYTE, status.MPI_SOURCE, 1, MPI_COMM_WORLD);
			printf("[worker%d] [%.1fs] terminated\n", status.MPI_SOURCE, seconds());
			fflush(stdout);
		}

		if(sigusr1_received){
			break;
		}


		while(id < blocks_size && blocks[id].processed){
			blocks[id].first_i = i+1;
			i += blocks[id].length;
			blocks[id].last_i = i;
			sum += blocks[id].partial_sum;
			blocks[id].sum = sum;

			printf("[summer] [%.1fs] block #%d computed\n", seconds(), id);
			printf("\tid=%d\n", id);
			printf("\tfirst_i=%" PRIu64 "\n", blocks[id].first_i);
			printf("\tlast_i=%" PRIu64 "\n", blocks[id].last_i);
			printf("\tlength=%" PRIu64 "\n", blocks[id].length);
			printf("\tfirst_prime=");
			print_uint128(blocks[id].first_prime);
			printf("\n\tlast_prime=");
			print_uint128(blocks[id].last_prime);
			printf("\n\tpartial_sum="); cout << blocks[id].partial_sum;
			printf("\n\tsum="); cout << blocks[id].sum;
			printf("\n\n");
			fflush(stdout);

			id++;
		}
	}
}

char * cmd_option(char ** begin, char ** end, const string & option) {
	char ** itr = std::find(begin, end, option);
	if(itr != end && ++itr != end){
		return *itr;
	}
	return 0;
}

bool cmd_option_exists(char** begin, char** end, const string& option){
	return std::find(begin, end, option) != end;
}

void root(int argc, char ** argv){
	if(cmd_option_exists(argv, argv+argc, "--block-boundary-db")) {
		char * filename = cmd_option(argv, argv + argc, "--block-boundary-db");
		if(std::filesystem::file_size(filename) > 0){
			// there's some pre-computed data
			block_boundary_db_file.open(filename, std::fstream::in);
		}else{
			cerr << "Empty boundary DB" << endl;
			MPI_Abort(MPI_COMM_WORLD, 1);
		}
		if(!block_boundary_db_file.is_open()){
			cerr << "Failed to open file " << filename << endl;
			MPI_Abort(MPI_COMM_WORLD, 1);
		}
	}else{
		cerr << "Please supply --block-boundary-db\n";
		MPI_Abort(MPI_COMM_WORLD, 1);
	}


	init();

	block_boundary_db_file.close();


	size_t id = 0;
	// load previously computed values
	if(cmd_option_exists(argv, argv+argc, "--continue")) {
		char * filename = cmd_option(argv, argv + argc, "--continue");
		ifstream file(filename);
		if(!file.is_open()){
			cerr << "Failed to open file " << filename << endl;
			MPI_Abort(MPI_COMM_WORLD, 1);
		}
		std::string line;
		std::regex num_rgx("^\t.+=(\\d+)$");
		std::regex id_rgx("^\tid=(\\d+)$");



		while(getline(file, line)){
			std::smatch match;
			if(std::regex_search(line, match, id_rgx)){
				id = std::stoull(match[1]);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].first_i = std::stoull(match[1]);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].last_i = std::stoull(match[1]);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].length = std::stoull(match[1]);
				getline(file, line); getline(file, line);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].partial_sum = match.str(1);
				getline(file, line); std::regex_search(line, match, num_rgx);
				blocks[id].sum = match.str(1);
				blocks[id].processed = true;

			}
		}
		file.close();
		printf("[main] [%.1fs] pre-computed blocks loaded. Continuing from #%d\n", seconds(), id+1);
		fflush(stdout);
	}

	summer(id);
}

int main(int argc, char ** argv){
	start = std::chrono::high_resolution_clock::now();

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

	std::signal(SIGINT, sigusr1_handler);
	if(my_rank == 0){
		root(argc, argv);
	}else{
		worker();
	}

	MPI_Finalize();
	return 0;

}
