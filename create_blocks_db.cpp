/**
 * Convert text based blocks DB to H5
 * usage:   make &&./create_blocks_db --block-boundary-db boundaries.txt --block-db blocks.txt --out a.h5
 */

#include <iostream>
#include <mp++/integer.hpp>
#include <string>
#include <vector>
#include <array>
#include <regex>
#include <fstream>
#include <filesystem>
#include <highfive/H5File.hpp>


using namespace mppp::literals;


typedef mppp::integer<3> sum_t;

// id: index of block from 0
// primes counted in range [p_{first_i}, p_{last_i}]
// length: last_i - first_i + 1
// partial_sum: sum within block
// sum: accumulative sum, including all previous blocks
// status: bitmap:
//		0b0000001: first_prime, last_prime started being computed
//		0b0000010: first_prime, last_prime computed
//		0b0000100: partial_sum, length started being computed
//		0b0001000: partial_sum, length computed
//		0b0010000: first_i, last_i, sum computed
//		0b0100000: A111441 started being checked
//		0b1000000: A111441 checked

#define STATUS_FIRST_PRIME_LAST_PRIME_STARTED_BEING_COMPUTED	(uint32_t(1) << 0)
#define STATUS_FIRST_PRIME_LAST_PRIME_COMPUTED					(uint32_t(1) << 1)
#define STATUS_PARTIAL_SUM_LENGTH_STARTED_BEING_COMPUTED		(uint32_t(1) << 2)
#define STATUS_PARTIAL_SUM_LENGTH_COMPUTED						(uint32_t(1) << 3)
#define STATUS_FIRST_I_LAST_I_SUM_COMPUTED						(uint32_t(1) << 4)
#define STATUS_A111441_STARTED_BEING_CHECKED					(uint32_t(1) << 5)
#define STATUS_A111441_CHECKED									(uint32_t(1) << 6)

typedef struct  {
	size_t id = 0;
	uint64_t first_i = 0;
	uint64_t last_i = 0;
	uint64_t length = 0;
	uint64_t first_prime = 0;
	uint64_t last_prime = 0;
	std::array<unsigned char, 3*(64/8)> partial_sum = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	std::array<unsigned char, 3*(64/8)> sum = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	uint32_t status = 0;
} h5_block_t;


HighFive::CompoundType create_compound_block() {
	return {
		{"id", HighFive::create_datatype<size_t>()},
		{"first_i", HighFive::create_datatype<uint64_t>()},
		{"last_i", HighFive::create_datatype<uint64_t>()},
		{"length", HighFive::create_datatype<uint64_t>()},
		{"first_prime", HighFive::create_datatype<uint64_t>()},
		{"last_prime", HighFive::create_datatype<uint64_t>()},
		{"partial_sum", HighFive::create_datatype<char [3*(64/8)]>()},
		{"sum", HighFive::create_datatype<char [3*(64/8)]>()},
		{"status", HighFive::create_datatype<uint32_t>()}
	};
}

HIGHFIVE_REGISTER_TYPE(h5_block_t, create_compound_block);


char * cmd_option(char ** begin, char ** end, const std::string & option) {
	char ** itr = std::find(begin, end, option);
	if(itr != end && ++itr != end){
		return *itr;
	}
	return nullptr;
}

bool cmd_option_exists(char** begin, char** end, const std::string& option){
	return std::find(begin, end, option) != end;
}

std::array<unsigned char, 3*(64/8)> mpz_to_byte_array(const sum_t a){
	const auto mpz_view = a.get_mpz_view();
	const mp_limb_t * limbs =  mpz_limbs_read(mpz_view);

	if(mpz_size(mpz_view) > 3){
		std::cerr << "too many limbs: " << a << std::endl;
		exit(1);
	}

	unsigned char buff[25];
	const auto written_characters = mpn_get_str(buff, 256, (mp_limb_t *) mpz_limbs_read(mpz_view), mpz_size(mpz_view));

	std::array<unsigned char, 24> out;
	out.fill(0);
	memcpy((void *) &(out[24-written_characters]), (void*) buff, written_characters);
	return out;
}

inline void collect_flag_range(const h5_block_t& block, const uint32_t flag, size_t& counter){
	if(
		(block.status & flag) &&
		block.id == counter
	){
		counter++;
	}
}

int main(int argc, char ** argv){
	std::fstream block_boundary_db_file;
	if(cmd_option_exists(argv, argv+argc, "--block-boundary-db")) {
		char * filename = cmd_option(argv, argv + argc, "--block-boundary-db");
		if(std::filesystem::file_size(filename) > 0){
			// there's some pre-computed data
			block_boundary_db_file.open(filename, std::fstream::in);
		}else{
			std::cerr << "Empty boundary DB" << std::endl;
			exit(1);
		}
		if(!block_boundary_db_file.is_open()){
			std::cerr << "Failed to open file " << filename << std::endl;
			exit(1);
		}
	}else{
		std::cerr << "Please supply --block-boundary-db\n";
		exit(1);
	}

	std::ifstream blocks_file;
	if(cmd_option_exists(argv, argv+argc, "--block-db")) {
		char * filename = cmd_option(argv, argv + argc, "--block-db");
		blocks_file.open(filename);
		if(!blocks_file.is_open()){
			std::cerr << "Failed to open file " << filename << std::endl;
			exit(1);
		}
	}else{
		std::cerr << "Please supply --block-db\n";
		exit(1);
	}

	const char * output_filename = cmd_option(argv, argv + argc, "--out");
	if(output_filename == nullptr){
		std::cerr << "No output file name supplied\n";
		exit(1);
	}

	HighFive::File output_file(output_filename, HighFive::File::Overwrite | HighFive::File::Create);

	auto t1 = create_compound_block();
	t1.commit(output_file, "Block");



	// load bondaries
	std::vector<h5_block_t> blocks;
	block_boundary_db_file.seekg(0, block_boundary_db_file.beg);

	std::string line{};
	std::regex rgx("^IMAX=(\\d+), CYCLE=(\\d+)$");
	std::getline(block_boundary_db_file, line);

	std::smatch match;
	if(std::regex_search(line, match, rgx)){
		const auto imax = std::stoull(match[1]);
		const auto cycle = std::stoull(match[2]);

		output_file.createAttribute("IMAX", imax);
		output_file.createAttribute("CYCLE", cycle);

		blocks.resize(imax/cycle + 1);
	}else{
		std::cerr << "Boundaries cycles not supplied\n";
		exit(1);
	}




	//consume empy line and check CSV header
	if(!std::getline(block_boundary_db_file, line) || !std::getline(block_boundary_db_file, line) || line != "id,first_prime,last_prime"){
		std::cerr << "Wrong boundary DB format\n";
		exit(1);
	}

	size_t block_id = 0;

	std::regex line_rgx("^(\\d+),(\\d+),(\\d+)$");
	while(std::getline(block_boundary_db_file, line)){
		if(!std::regex_search(line, match, line_rgx)){
			break;
		}


		const size_t read_id = std::stoull(match[1]);
		const size_t read_first_prime = std::stoull(match[2]);
		const size_t read_last_prime = std::stoull(match[3]);
		

		if(read_id != block_id){
			std::cerr << "Wrong boundary DB data\n";
			exit(1);
		}

		blocks[block_id].first_prime = read_first_prime;
		blocks[block_id].last_prime = read_last_prime;
		blocks[block_id].id = block_id;

		blocks[block_id].status |= STATUS_FIRST_PRIME_LAST_PRIME_STARTED_BEING_COMPUTED | STATUS_FIRST_PRIME_LAST_PRIME_COMPUTED;


		if(read_first_prime > read_last_prime || (block_id>0 && blocks[block_id-1].last_prime >= read_first_prime)){
			std::cerr << "Wrong boundary DB data\n";
			exit(1);
		}

		block_id++;
	}

	blocks.resize(block_id);

	block_boundary_db_file.close();


	size_t id = 0;
	// load previously computed values from block db
	{
		std::string line;
		std::regex num_rgx("^\t.+=(\\d+)$");
		std::regex id_rgx("^\tid=(\\d+)$");

		while(std::getline(blocks_file, line)){
			std::smatch match;
			if(std::regex_search(line, match, id_rgx)){
				id = std::stoull(match[1]);
				if(id >= blocks.size()){
					continue;
				}

				std::getline(blocks_file, line); std::regex_search(line, match, num_rgx);
				blocks[id].first_i = std::stoull(match[1]);
				std::getline(blocks_file, line); std::regex_search(line, match, num_rgx);
				blocks[id].last_i = std::stoull(match[1]);
				std::getline(blocks_file, line); std::regex_search(line, match, num_rgx);
				blocks[id].length = std::stoull(match[1]);
				std::getline(blocks_file, line); std::regex_search(line, match, num_rgx);
				if(std::stoull(match[1]) != blocks[id].first_prime){
					std::cerr << "wrong first prime at id: " << id << std::endl;
					exit(1);
				}
				std::getline(blocks_file, line); std::regex_search(line, match, num_rgx);
				if(std::stoull(match[1]) != blocks[id].last_prime){
					std::cerr << "wrong last prime at id: " << id << std::endl;
					exit(1);
				}
				std::getline(blocks_file, line); std::regex_search(line, match, num_rgx);
				sum_t partial_sum = 0; partial_sum = match.str(1);
				blocks[id].partial_sum = mpz_to_byte_array(partial_sum);
				std::getline(blocks_file, line); std::regex_search(line, match, num_rgx);
				sum_t sum = 0; sum = match.str(1);
				blocks[id].sum = mpz_to_byte_array(sum);
				blocks[id].status |= STATUS_PARTIAL_SUM_LENGTH_STARTED_BEING_COMPUTED | STATUS_PARTIAL_SUM_LENGTH_COMPUTED | STATUS_FIRST_I_LAST_I_SUM_COMPUTED;
			}
		}
		blocks_file.close();
	}




	output_file.createDataSet("/blocks",blocks);


	// quick indices to show what has been fully covered in range [0, __until)
	size_t first_prime_last_prime_started_being_computed_until	= 0;
	size_t first_prime_last_prime_computed_until				= 0;
	size_t partial_sum_length_started_being_computed_until		= 0;
	size_t partial_sum_length_computed_until					= 0;
	size_t first_i_last_i_sum_computed_until					= 0;
	size_t a111441_started_being_checked_until					= 0;
	size_t a111441_checked_until								= 0;

	for(const auto& block: blocks){
		collect_flag_range(block, STATUS_FIRST_PRIME_LAST_PRIME_STARTED_BEING_COMPUTED	, first_prime_last_prime_started_being_computed_until	);
		collect_flag_range(block, STATUS_FIRST_PRIME_LAST_PRIME_COMPUTED				, first_prime_last_prime_computed_until					);
		collect_flag_range(block, STATUS_PARTIAL_SUM_LENGTH_STARTED_BEING_COMPUTED		, partial_sum_length_started_being_computed_until		);
		collect_flag_range(block, STATUS_PARTIAL_SUM_LENGTH_COMPUTED					, partial_sum_length_computed_until						);
		collect_flag_range(block, STATUS_FIRST_I_LAST_I_SUM_COMPUTED					, first_i_last_i_sum_computed_until						);
		collect_flag_range(block, STATUS_A111441_STARTED_BEING_CHECKED					, a111441_started_being_checked_until					);
		collect_flag_range(block, STATUS_A111441_CHECKED								, a111441_checked_until									);
	}

	output_file.createAttribute<size_t>("first_prime_last_prime_started_being_computed_until"	, first_prime_last_prime_started_being_computed_until	);
	output_file.createAttribute<size_t>("first_prime_last_prime_computed_until"					, first_prime_last_prime_computed_until					);
	output_file.createAttribute<size_t>("partial_sum_length_started_being_computed_until"		, partial_sum_length_started_being_computed_until		);
	output_file.createAttribute<size_t>("partial_sum_length_computed_until"						, partial_sum_length_computed_until						);
	output_file.createAttribute<size_t>("first_i_last_i_sum_computed_until"						, first_i_last_i_sum_computed_until						);
	output_file.createAttribute<size_t>("a111441_started_being_checked_until"					, a111441_started_being_checked_until					);
	output_file.createAttribute<size_t>("a111441_checked_until"									, a111441_checked_until									);

	output_file.flush();

	return 0;
}