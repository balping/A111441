/*
    Compute OEIS A111441 entries
    See https://oeis.org/A111441

    Copyright (C) 2020  Balázs Dura-Kovács

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// https://github.com/kimwalisch/primesieve
// https://bluescarni.github.io/mppp/installation.html
// https://oeis.org/A111441/list
// g++ -Ofast parallel_from_db.cpp -o parallel_from_db -lprimesieve -lmp++ -lgmp -pthread
// ./parallel_from_db counts.txt start_block_id | tee -a results_parallel_from_db2.txt

#include <stdio.h>
#include <string>
#include <regex>
#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <stdlib.h>
#include <sys/stat.h>
#include <primesieve.hpp>
#include <mp++/integer.hpp>

#define NUM_WORKERS  10 // how many worker threads to use (+1 for the counter)


using namespace std;

typedef unsigned __int128 __uint128;
typedef mppp::integer<3> sum_t;
const __uint128 UINT128_MAX = ~((__uint128)0);

// id: index of block from 0
// primes counted in range [p_{first_i}, p_{last_i}]
// starting_sum: sum of all previous blocks, but not this one
typedef struct  {
	size_t id;
	uint64_t first_i;
	uint64_t last_i;
	__uint128 first_prime;
	__uint128 last_prime;
	sum_t starting_sum;
	sum_t sum;
} block_t;

typedef enum {
	IDLE, RUNNING, STOPPED
} worker_status_t;

worker_status_t worker_status[NUM_WORKERS];

mutex queue_mtx;
mutex status_mtx;
mutex io_mtx;
queue<block_t *> blocks;
condition_variable cv;

struct {
	string name;
	ifstream stream;
	time_t modified;
} file;

bool counter_finished = false;
bool toexit = false;


std::chrono::_V2::system_clock::time_point start;

void counter(size_t start_block);
void worker(int);
void scan(block_t * block, int id);
double seconds();

void print_uint128(__uint128 n);
void print_status(uint64_t i, __uint128 prime, sum_t sum);

// thread to load sums from file until
// start of blocks
void counter(size_t start_block){
	std::string line;
	const std::regex num_rgx("^\t.+=(\\d+)$");
	const std::regex id_rgx("^\tid=(\\d+)$");

	sum_t starting_sum{0};
	size_t id;
	block_t * block;

	unique_lock<mutex> ul(queue_mtx);
	ul.unlock();


	std::smatch match;
	while(!toexit){
		if(!getline(file.stream, line)){
			file.stream.close();
			std::this_thread::sleep_for(std::chrono::seconds(30));
			// if file was modified, re-open and seek to last
			// loaded block
			struct stat r; stat(file.name.c_str(), &r);
			time_t modified = r.st_mtime;
			if(modified > file.modified){
				file.modified = modified;
				file.stream.open(file.name);
				while(getline(file.stream, line)){
					if(line == "\tid=" + to_string(id)){
						break;
					}
				}
				continue;
			}
		}

		if(std::regex_search(line, match, id_rgx)){
			id = std::stoull(match[1]);

			// need one block before start
			if(id+1 < start_block){
				continue;
			}

			block = new block_t;
			block->id = id;

			getline(file.stream, line); std::regex_search(line, match, num_rgx);
			block->first_i = std::stoull(match[1]);
			
			getline(file.stream, line); std::regex_search(line, match, num_rgx);
			block->last_i = std::stoull(match[1]);
			
			getline(file.stream, line); //length
			
			getline(file.stream, line); std::regex_search(line, match, num_rgx);
			block->first_prime = std::stoull(match[1]);

			getline(file.stream, line); std::regex_search(line, match, num_rgx);
			block->last_prime = std::stoull(match[1]);

			getline(file.stream, line); //partial sum
			
			getline(file.stream, line); std::regex_search(line, match, num_rgx);
			block->starting_sum = starting_sum; // end sum from previous block
			starting_sum = match.str(1);
			block->sum = starting_sum;

			if(id >= start_block){
				io_mtx.lock();
					printf("[counter] [%.1fs] block #%d loaded\n", seconds(), id);
					fflush(stdout);
				io_mtx.unlock();

				ul.lock();
					blocks.push(block);
					cv.notify_one();
				ul.unlock();
			}

		}
	}

	io_mtx.lock();
		printf("[counter] [%.1fs] Finished\n", seconds());
		fflush(stdout);
	io_mtx.unlock();

	ul.lock();
	notify_all_at_thread_exit(cv,std::move(ul));
	counter_finished = true;
}

// process items in the block queue
void worker(int id){
	while(true){
		status_mtx.lock();
			worker_status[id] = IDLE;
		status_mtx.unlock();

		// if there are no more blocks to process, quit
		if(counter_finished){
			queue_mtx.lock();
			bool empty = blocks.empty();
			queue_mtx.unlock();
			if(empty){
				io_mtx.lock();
					printf("[worker%d] [%.1fs] finished at loop start\n", id, seconds());
					fflush(stdout);
				io_mtx.unlock();
				status_mtx.lock();
					worker_status[id] = STOPPED;
				status_mtx.unlock();
				return;
			}
		}

		unique_lock<mutex> ul(queue_mtx);
		// if there's a block available, process it,
		// otherwise wait for a new one
		if(blocks.empty()){
			cv.wait(ul, [=](){ return !blocks.empty() || counter_finished; });
			if(counter_finished && blocks.empty()){
				ul.unlock();
				io_mtx.lock();
					printf("[worker%d] [%.1fs] finished after signal\n", id, seconds());
					fflush(stdout);
				io_mtx.unlock();
				status_mtx.lock();
					worker_status[id] = STOPPED;
				status_mtx.unlock();
				return;
			}
		}
		block_t * block = blocks.front();
		blocks.pop();
		ul.unlock();

		status_mtx.lock();
			worker_status[id] = RUNNING;
		status_mtx.unlock();

		scan(block, id);
		delete block;
	}
}

#define DIVISIBLE(n, d)  mpz_divisible_ui_p((n).get_mpz_view(), (d))


// scans a block to find matches
void scan(block_t * block, int id){
	primesieve::iterator it;
	__uint128 prime;

	uint64_t i = block->first_i;
	it.skipto(block->first_prime-1);


	// 192bit signed int -> overflow protected
	sum_t sum = block->starting_sum;
	// reduce number of mppp operations by using
	// partial sum on 128bit int inside the loop
	__uint128 partial_sum = 0;

	io_mtx.lock();
		printf("[worker%d] [%.1fs] Scanning of #%d started %lu – %lu",
			id, seconds(), block->id, block->first_i, block->last_i);
		print_status(i-1, 0, sum);
		fflush(stdout);
	io_mtx.unlock();

	// loop until we get i % 6 == 0
	do{
		prime = it.next_prime();
		sum += prime*prime;
		if(DIVISIBLE(sum, i)){
			io_mtx.lock();
				printf("[counter] [%.1fs] ",seconds());
				printf("match: %lu\n", i);
				fflush(stdout);
			io_mtx.unlock();
		}
		i++;
	}while(i % 6 != 0);

	const uint64_t nextstop = block->last_i - 6;


	for(; i<=nextstop; i+=6){
		// i % 6 == 0: skip check
		prime = it.next_prime();
		partial_sum = prime*prime;
		
		// i % 6 == 1: check
		prime = it.next_prime();
		partial_sum += prime*prime;
		sum += partial_sum;
		if(DIVISIBLE(sum, i+1)){
			io_mtx.lock();
				printf("[worker%d] [%.1fs] match: %lu\n", id, seconds(), i+1);
				fflush(stdout);
			io_mtx.unlock();
		}

		// i % 6 == 2: skip check
		prime = it.next_prime();
		partial_sum = prime*prime;

		// i % 6 == 3: skip check
		prime = it.next_prime();
		partial_sum += prime*prime;

		// i % 6 == 4: skip check
		prime = it.next_prime();
		partial_sum += prime*prime;

		// i % 6 == 5:  check
		prime = it.next_prime();
		partial_sum += prime*prime; // WARNING: this overflows if prime > 9.2*10^18
		sum += partial_sum;
		if(DIVISIBLE(sum, i+5)){
			io_mtx.lock();
				printf("[worker%d] [%.1fs] match: %lu\n", id, seconds(), i+5);
				fflush(stdout);
			io_mtx.unlock();
		}
	}

	// loop until we reach the end of the block
	do{
		prime = it.next_prime();
		sum += prime*prime;
		if(DIVISIBLE(sum, i)){
			io_mtx.lock();
				printf("[counter] [%.1fs] ",seconds());
				printf("match: %lu\n", i);
				fflush(stdout);
			io_mtx.unlock();
		}
		i++;
	}while(i <= block->last_i);

	io_mtx.lock();
		printf("[worker%d] [%.1fs] Scanning finished of #%d", id, seconds(), block->id);
		if(sum != block->sum || prime != block->last_prime || i-1 != block->last_i){
			printf("[worker%d] [%.1fs] ERROR at #%d", id, seconds(), block->id);
		}
		print_status(i-1, prime, sum);
		fflush(stdout);
	io_mtx.unlock();

}

void commandline(){
	string command;
	while(true){
		getline(cin, command);
		if(command == "exit"){
			io_mtx.lock();
				printf("[command] [%.1fs] exit recieved\n", seconds());
				fflush(stdout);
			io_mtx.unlock();
			queue_mtx.lock();
				// clear queue
				while (!blocks.empty()){
					blocks.pop();
				}
				toexit = true;
			queue_mtx.unlock();
			return;
		}else if(command == "info"){
			size_t idle_count = 0;
			size_t running_count = 0;
			size_t stopped_count = 0;
			size_t unprocessed;
			bool counter_running;

			queue_mtx.lock();
			status_mtx.lock();
				for(size_t i=0; i<NUM_WORKERS; i++){
					switch(worker_status[i]){
						case IDLE:
							idle_count++;
							break;
						case RUNNING:
							running_count++;
							break;
						case STOPPED:
							stopped_count++;
							break;
					}
				}
				unprocessed = blocks.size();
				counter_running = !counter_finished;
			status_mtx.unlock();
			queue_mtx.unlock();

			io_mtx.lock();
				printf("[command] [%.1fs] info\n", seconds());
				printf("\trunning: %d, idle: %d, stopped: %d\n",
					running_count, idle_count, stopped_count);
				printf("\tblocks in queue: %d\n", unprocessed);
				printf("\tcounter is ");
				if(!counter_running){ cout << "not "; }
				printf("running\n");
				fflush(stdout);
			io_mtx.unlock();

		}
	}
}


int main(int argc, char ** argv){
	start = std::chrono::high_resolution_clock::now();

	if(argc != 3){
		printf("Usage: %s counts_database block_id\n", argv[0]);
		return -1;
	}

	file.name = argv[1];
	file.stream.open(file.name);
	if(!file.stream.is_open()){
		cerr << "Failed to open file " << file.name << endl;
		return -1;
	}
	struct stat r; stat(file.name.c_str(), &r);
	file.modified = r.st_mtime;
	size_t start_block = std::stoull(argv[2]);


	thread th_commandline(commandline);
	thread th_counter(counter, start_block);
	thread th_workers[NUM_WORKERS];

	for(size_t i=0; i<NUM_WORKERS; i++){
		th_workers[i] = thread(worker,i);
	}

	th_commandline.join();
	th_counter.join();
	for (auto& th : th_workers) th.join();

	file.stream.close();

	return 0;
}


void print_uint128(__uint128 n){
	if(n == 0){
		return;
	}

	print_uint128(n/10);
	putchar(n%10+'0');
}

double seconds(){
	auto now = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diff = now - start;
	return diff.count();
}

void print_status(uint64_t i, __uint128 prime, sum_t sum){
	printf("\n\ti=%lu", i);
	printf("\n\tlast_prime=");
	print_uint128(prime);
	printf("\n\tsum=");
	cout << sum;
	printf("\n");
}
