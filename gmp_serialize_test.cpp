#include <iostream>
#include <mp++/integer.hpp>
#include <string>

using namespace std;
using namespace mppp::literals;


typedef mppp::integer<3> sum_t;

int main(){
    sum_t a = 34535_z3;

    const mp_limb_t * limbs =  mpz_limbs_read(a.get_mpz_view());
    //cout << a << "\n";
    cout << mpz_size(a.get_mpz_view()) << "\n";

    const auto mpz_view = a.get_mpz_view();

    if(mpz_size(mpz_view) > 3){
        cerr << "too many limbs\n";
        exit(1);
    }

    unsigned char out[mp_bits_per_limb / 8 * mpz_size(mpz_view) + 1];


    const auto written_characters = mpn_get_str(out, 256, (mp_limb_t *) mpz_limbs_read(mpz_view), mpz_size(mpz_view));

    cout << written_characters << "\n\n\n";

    for(int i=0; i<written_characters; i++){
        cout << (unsigned int) out[i] << "\n";
    }

    cout << "\n\n";


    mp_limb_t rp[4] = {0,0,0,0};
    //const mp_size_t read_limbs = mpn_set_str(rp, out, written_characters, 256);
    const size_t raw_array_length = mp_bits_per_limb / 8 * 3; 
    unsigned char out2[raw_array_length] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,204};
    int offset = 0;
    while(offset < raw_array_length - 1 && out2[offset] == 0){
        offset++;
    }
    const mp_size_t read_limbs = mpn_set_str(rp, out2+offset, raw_array_length-offset, 256);
    cout << read_limbs << "\n";

    for(int i=0; i<4; i++){
        cout << rp[i] << "\n";
    }

    mpz_t readback = MPZ_ROINIT_N(rp, read_limbs);

    cout << readback->_mp_alloc << "\n";

    sum_t readback_mppp{readback};

    cout << readback_mppp << "\n";
    readback_mppp++;

    for(int i=0; i<4; i++){
        cout << rp[i] << "\n";
    }

    cout << readback_mppp << "\n";



}