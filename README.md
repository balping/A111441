# A111441

Efficient code to calculate entries for [OEIS A111441](https://oeis.org/A111441)

## Dependencies

* gcc supporting at least C++14
* [primesieve](https://github.com/kimwalisch/primesieve)
* [GMP](https://gmplib.org/)
* [mp++](https://bluescarni.github.io/mppp/installation.html)

## Building and running

This program can be run on multiple CPU cores. Adjust `NUM_WORKERS` in `just_count.cpp` and `parallel_from_db.cpp` to fit your needs.

First compile `just_count`:

```
g++ -O3 just_count.cpp -o just_count -lprimesieve -lmp++ -lgmp -pthread
```

Then you start it to calculate checkpoints and save it to `counts.txt`:

```
./just_count | tee -a counts.txt
```

If you inturrupt the calculation, you can continue from the last checkpoint:

```
./just_count --continue counts.txt | tee -a counts.txt
```

Once there are blocks in `counts.txt`, run `parallel_from_db`, which checks if a number is member of the sequence:

```
g++ -Ofast parallel_from_db.cpp -o parallel_from_db -lprimesieve -lmp++ -lgmp -pthread
./parallel_from_db counts.txt start_block_id | tee -a results_parallel_from_db2.txt
```

You can find pre-calculated checkpoints in the `data/` folder, so you don't need to start from scratch.

where `start_block_id` is the id of the block in `counts.txt` where you would like to start the computation from. Any results will be printed in a line containing "`match:`".

## Building with Cmake

Alternatively, after installing GMP, you can build like this:

```sh
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

## License

Released under GPLv3.