(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 13.1' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16345,        433]
NotebookOptionsPosition[     13824,        386]
NotebookOutlinePosition[     14226,        402]
CellTagsIndexPosition[     14183,        399]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[{
 Cell[BoxData[
  FormBox["n", TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "eae9db66-34f7-489f-a7fe-eb4b84d47760"],
 "th prime can be approximated as ",
 Cell[BoxData[
  FormBox[
   SubscriptBox["p", "n"], TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "9124928f-3dc4-45f3-bee6-9e6220b9b18e"],
 "\[TildeTilde]n log(n)"
}], "Text",
 CellChangeTimes->{{3.874681633728606*^9, 3.874681676305667*^9}, {
  3.8746817203218603`*^9, 
  3.8746817226752367`*^9}},ExpressionUUID->"1246511a-6110-4563-8c9d-\
88861b0ff3d8"],

Cell[TextData[{
 "The sum up to the ",
 Cell[BoxData[
  FormBox["n", TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "fec6618d-a1e4-4cd9-81c3-60af33729ad4"],
 "th prime is approximately"
}], "Text",
 CellChangeTimes->{{3.874681699514225*^9, 3.874681700026229*^9}, {
  3.874681732218442*^9, 
  3.874681761907112*^9}},ExpressionUUID->"edae2ced-4e89-4d18-b92b-\
a225d0657680"],

Cell[BoxData[
 RowBox[{
  SubsuperscriptBox["\[Integral]", "1", "n"], 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{"k", "*", 
      RowBox[{"Log", "[", "k", "]"}]}], ")"}], "2"], 
   RowBox[{"\[DifferentialD]", "k"}]}]}]], "Input",
 CellChangeTimes->{
  3.874682058447359*^9},ExpressionUUID->"de8e58b6-445f-467e-859d-\
236c912c521d"],

Cell["\<\
The largest n we can sum up to, given the number of bits to store the sum\
\>", "Text",
 CellChangeTimes->{{3.874682085887333*^9, 
  3.874682115191502*^9}},ExpressionUUID->"3daa7ba1-f2e2-489a-8c91-\
14a234ddbd91"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"N", "@", 
  RowBox[{"Solve", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"nn", ">", "0"}], "&&", 
     RowBox[{
      RowBox[{"Log", "[", 
       RowBox[{"2", ",", 
        RowBox[{
         SubsuperscriptBox["\[Integral]", "1", "nn"], 
         RowBox[{
          SuperscriptBox[
           RowBox[{"(", 
            RowBox[{"k", "*", 
             RowBox[{"Log", "[", "k", "]"}]}], ")"}], "2"], 
          RowBox[{"\[DifferentialD]", "k"}]}]}]}], "]"}], "==", 
      RowBox[{
       RowBox[{"3", "*", "64"}], "-", "1"}]}]}], ",", 
    RowBox[{"{", "nn", "}"}], ",", "Reals"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.8746834766150827`*^9, 3.874683476826005*^9}},
 CellLabel->"In[49]:=",ExpressionUUID->"25600010-176a-4efb-b39d-f56e0758082a"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"nn", "\[Rule]", "1.7566264023474785`*^18"}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.874682125925954*^9, 3.874683478610167*^9},
 CellLabel->"Out[49]=",ExpressionUUID->"4ae815a0-5bee-465c-ad8a-4bc61200daaa"]
}, Open  ]],

Cell["Same with better approximation", "Text",
 CellChangeTimes->{{3.8746825163875103`*^9, 
  3.874682525634609*^9}},ExpressionUUID->"26b9cecc-9a8f-45e0-8fe2-\
65aa2712ab89"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Papprx", "[", "i_", "]"}], ":=", 
  RowBox[{"i", "*", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"Log", "[", "i", "]"}], "+", 
     RowBox[{"Log", "[", 
      RowBox[{"Log", "[", 
       RowBox[{"i", "-", "1"}], "]"}], "]"}], "+", 
     FractionBox[
      RowBox[{"Log", "[", 
       RowBox[{"Log", "[", 
        RowBox[{"i", "-", "2"}], "]"}], "]"}], 
      RowBox[{"Log", "[", "i", "]"}]], "-", 
     FractionBox[
      RowBox[{
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"Log", "[", 
          RowBox[{"Log", "[", "i", "]"}], "]"}], ")"}], "2"], "-", 
       RowBox[{"6", "*", 
        RowBox[{"Log", "[", 
         RowBox[{"Log", "[", 
          RowBox[{"i", "+", "11"}], "]"}], "]"}]}]}], 
      RowBox[{"2", "*", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"Log", "[", "i", "]"}], ")"}], "2"]}]]}], ")"}]}]}]], "Input",\

 CellLabel->"In[26]:=",ExpressionUUID->"1bb6ce35-deaa-42fc-9e0c-17e2907aed10"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"Log", "[", 
     RowBox[{"2", ",", 
      RowBox[{"NIntegrate", "[", 
       RowBox[{
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"Papprx", "[", "k", "]"}], ")"}], "2"], ",", 
        RowBox[{"{", 
         RowBox[{"k", ",", "1", ",", "nn"}], "}"}]}], "]"}]}], "]"}], "-", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"3", "*", "64"}], "-", "1"}], ")"}]}], ",", 
   RowBox[{"{", 
    RowBox[{"nn", ",", "2", ",", 
     RowBox[{"4", "*", 
      RowBox[{"10", "^", "18"}]}]}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.874682785795334*^9, 3.874682907931777*^9}, {
  3.8746829479350452`*^9, 3.874682948115453*^9}, {3.8746830029984007`*^9, 
  3.8746830549549427`*^9}, {3.8746830961435223`*^9, 3.874683098637545*^9}, {
  3.874683497932859*^9, 3.874683500882763*^9}},
 CellLabel->"In[50]:=",ExpressionUUID->"7c991eee-99f3-4b6b-88ed-c88bd121f574"],

Cell[BoxData[
 GraphicsBox[{{{}, {}, 
    TagBox[
     {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
      1.], LineBox[CompressedData["
1:eJwV0Xs41GkbB/AfIsTPYCtn0zhLbaKieOfhbt9YwtJBOU0OpbathnGIDoOY
nEaLpnWI2WLURIhSwjxIDVO97W5K8jp3prS8WIe1v/eP53quz/W97+u+nvtZ
E3rcN0KeIIg46vz/Xrtx4UhgOhvWdydlb2q3xhlRA45ljWzYwLbvzxZb46x0
qyVnpSgQ+t20D0i1xrkNzensy1FAqI2GtTpZ42LttyU9smjwYa+Vc7xlhW/J
NneKLGNhwsIJHai3xLeHuXzEiQXh0HfBuQJLfPevLr9eSSxgr2PbLE5a4maL
kH71vXFQ0zRGRDEtsTSFN8lJjQciBAncn1ng/m09hjCcAEjcudJOzgIP+TJG
+tYlgrDaM6fwvTkePXz0WuzJRKDLRej/+5k5/nCJsBPTToHwD7+yMKE5npq0
dNNkngafsayUXDDHqpUnowaKzgLRI5dQk2+G9fnZI83WyYBjxF+1AkzxVMYA
V8UrGegLwjn1703xY56t0W52MgirIu9aOZriM0nd/mMNyTC43j60dbUpHuYY
PtX7LgW4wbejw1+YYHFA1d24kHNw4tmyVt19JtjB6km2bX4aDE7vQ9VsBtY0
N157uiENhGPKe+fDGPgjgy2V9lL534LrgXsYuNhgpQJrDQ9YHsOLQdsYeEkj
KJ5/kwe4ff+5XUoM/HB6LPST9Dz4rF/g/7d0Dd71QM2hfDEDuGVnWFMDdDzX
suESk54Jg2dXqN95TsfCe7umX7lmwgZXvkpaJx2P3SyuJ89T9lo84FNHx+cK
bGzjtbJAaFTSmMGj4/pjntYeFtngw98zF7+RjrV1swy++uQAkX6O95xhjO9q
1yRmRueA0G0uk61hjIPJ571mghzg1tJemywYYbGCQcH+1zlA708ofdRthNfM
dKsOb7wAwtn4p0oZRvjeUATPo/kCENtal5ynDfGHO6lco99/hkFNuyeJfQb4
F4mZzsjoz8BK3QJyXQbYTdpRXTFD5XHxWoUNBlj0SnFgg2EuCL279BXzDHDo
QqozRObCYBaH3/S9Ae51SZs79Hcu0C9V6l9o1sePH6dxaq3zAflPzfRU6+Gb
w7zI7SkCwIwZKR/rYJXwM0HSfAGwgnwS397SweFvOL6eIgEQAcltbuU6WO/9
AaddnZQ/b3pqk6GDU8e30cI1LgE997Aee5cODpz90pBSRPnLjtm+j6uxCum/
vL3uF2BNbdrcZ7QahztailxGC4HwVS+4IVyJ6Yc83ulcLQWh+dE57w4tzLTY
aztqdgW4LJ1xgwAaPljzssDCtwwwer0lrkkN08p1l/lXiaj9tjhtHFLGdtXW
upzBa4A7G0U6QkVsIQjwOKV8A7in+bIV9fK45P3Os+yQKuA6Vyqh3CVJto9G
sC+/GrgTujyN83MSQsvdmDZdA9wCh51NUf+TMI7oGf1kfAu4RsmudV0TEmKn
358S+zogDHNSxn/8ICl9c3RH+oF6IJZ3mc9qDUnmlT+z3RJuA6F8+WLu4T8k
OsPehmfEd4DYEP143apaScl1l5dW3Q1AcCrilqramYPzhQdln+4CYTl3VTvi
BXNEoTt2RLcRiNw3YcfqhphXBbGBX+3uA9H27BSkvGPKVl/ziXFrAsJh/9WE
hTHmba/GjuOcZuCmDTqK/L8yA2j2NFZWCxBbWalJ7yeZbTz+64f3JcDNq6X5
XZxmdsY29f4WhYH7ry27bQ3/Ymocj9h4fxEDceLCB0bEPHNz25mukPxWIBRS
v1R2LjLv93rsXKK3AVEleOU0ucTkfGS9fCVuA/Sgym+MkEPk7re/HkftQGRu
ybdZJY+UUo33Eo8pX9FNCtJUQAManZ6eYQ8AHQrJm9VdhlaxzNflTTwA4s+Q
xc2qiiipQvbNysQOQK0dSVVfFNET9yEnX82HQDxwkVszrITiasXeGZcfUvkB
8nrLciTPGb1yetMjwG1OopoKZdS67uDijS7KugNHW9JUUH9lkWv3bilwA5pP
fP1BFfWluUjvjUmB4FY42VutQA7mRQ4FJzsBZdOySsdXoM4mp5o6hS7qPSWZ
emI1tPWmV2R6QRfgSb1VjlHqSCXimxX7zWTUf66qVrIi0WaPEP8uCxlwd+Rn
r7chUaituHyrNZX3Jpvs+ZZETYtMpP+tDJDmm5iyTSQ6Ifgxps9BBrjW6o6z
K4l6HrUNBHtSrl5eExpAomvW7PqwaMoiYiQ2m0TdtCa55zFU/3CK9OIFEsnP
KHlvj6d8RfNQfR6JAtuLP5iepuZpn/3PRAGJaIFSo7dplK/LeR8UkSg+2zg9
spCye6H2dgmJyqOPvOgppvrDRTnBbST6fd9tE/dSap5Kakd8B4lszD1arMoo
J30KrpSRaLAldvJjFdXv0eBO6yGRenkrCqihPFt70fI1ibZmqvFlt6j6qaEi
1E+i/L1XLCsbKCtqvzw+SqJW5/EYg0aqvsxGjfeORJ9NHNqzmqh9VVTOX/5I
In3VFNpiC5V7lZbXj5PIbeJJ0E+tlH+jqcomSPQP+Gt64w==
       "]]},
     Annotation[#, "Charting`Private`Tag$199030#1"]& ]}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{
   "DefaultBoundaryStyle" -> Automatic, 
    "DefaultGraphicsInteraction" -> {
     "Version" -> 1.2, "TrackMousePosition" -> {True, False}, 
      "Effects" -> {
       "Highlight" -> {"ratio" -> 2}, "HighlightPoint" -> {"ratio" -> 2}, 
        "Droplines" -> {
         "freeformCursorMode" -> True, 
          "placement" -> {"x" -> "All", "y" -> "None"}}}}, "DefaultMeshStyle" -> 
    AbsolutePointSize[6], "ScalingFunctions" -> None, 
    "CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{2, 4000000000000000000}, {-13.381267950152132`, 
    3.8675747667676887`}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.8746828179820547`*^9, 3.874682908803053*^9}, 
   3.874682949027663*^9, {3.87468300560387*^9, 3.8746830107382402`*^9}, {
   3.874683081041513*^9, 3.874683099409276*^9}, 3.874683502226822*^9},
 CellLabel->"Out[50]=",ExpressionUUID->"3897d396-49b9-47f1-87ff-29bf3cdaf09d"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Log", "[", 
  RowBox[{"2", ",", 
   RowBox[{"NIntegrate", "[", 
    RowBox[{
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"Papprx", "[", "k", "]"}], ")"}], "2"], ",", 
     RowBox[{"{", 
      RowBox[{"k", ",", "1", ",", 
       SuperscriptBox["10", "18"]}], "}"}]}], "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.874683135165764*^9, 3.8746831434353724`*^9}},
 CellLabel->"In[44]:=",ExpressionUUID->"f7500038-837f-40e9-9d1a-aef5cf5c827b"],

Cell[BoxData["188.77796401320057`"], "Output",
 CellChangeTimes->{3.874683144140904*^9},
 CellLabel->"Out[44]=",ExpressionUUID->"fd173feb-d248-404e-9d42-b9586a53161a"]
}, Open  ]],

Cell[TextData[{
 "It should be safe to store the sum up to the ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["10", "18"], TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "919eb064-a4d2-4160-9027-9021263e1d33"],
 "th prime on 3*64-1 bits"
}], "Text",
 CellChangeTimes->{{3.87468325215646*^9, 3.874683293958661*^9}, {
  3.874683597595386*^9, 
  3.874683597650401*^9}},ExpressionUUID->"aa770861-3a59-429e-bf25-\
190c9a1fb76b"],

Cell["Check squaring on uint128", "Text",
 CellChangeTimes->{{3.874683612753598*^9, 
  3.874683640419532*^9}},ExpressionUUID->"f3da7bcb-8d53-4d92-a2de-\
6dc89639e16d"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Log", "[", 
   RowBox[{"2", ",", 
    RowBox[{"Papprx", "[", 
     SuperscriptBox["10", "18"], "]"}]}], "]"}], "//", "N"}]], "Input",
 CellChangeTimes->{{3.8746831545996513`*^9, 3.874683163909368*^9}, {
  3.874684267425514*^9, 3.8746842732682657`*^9}},
 CellLabel->"In[66]:=",ExpressionUUID->"e70ad79a-7c8b-4ac8-9ac9-2de2702a38be"],

Cell[BoxData["65.29497440050335`"], "Output",
 CellChangeTimes->{3.874683164491042*^9, 3.8746842753750467`*^9},
 CellLabel->"Out[66]=",ExpressionUUID->"88a7560a-2750-4831-9d90-d4416e4d6b9e"]
}, Open  ]],

Cell["More than 64, bad.", "Text",
 CellChangeTimes->{{3.874683698730255*^9, 3.874683717463628*^9}, {
  3.874684288595783*^9, 
  3.874684291683913*^9}},ExpressionUUID->"57f5920f-b14f-46b2-af51-\
eabf30ca39ca"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"NSolve", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"nn", ">", "0"}], "&&", 
    RowBox[{
     RowBox[{"Log", "[", 
      RowBox[{"2", ",", 
       RowBox[{"nn", "*", 
        RowBox[{"Log", "[", "nn", "]"}]}]}], "]"}], "==", "64"}]}], ",", 
   RowBox[{"{", "nn", "}"}], ",", "Reals"}], "]"}]], "Input",
 CellChangeTimes->{{3.874684306277721*^9, 3.8746843122044697`*^9}},
 CellLabel->"In[67]:=",ExpressionUUID->"1be41e46-90c9-427f-bd34-f21db754db79"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{"nn", "\[Rule]", "4.537244963409272`*^17"}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{3.874684313022852*^9},
 CellLabel->"Out[67]=",ExpressionUUID->"fb826548-01de-437a-adf8-7f24374215a8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Log", "[", 
   RowBox[{"2", ",", 
    RowBox[{"Papprx", "[", 
     RowBox[{"2", "*", 
      SuperscriptBox["10", "17"]}], "]"}]}], "]"}], "//", "N"}]], "Input",
 CellChangeTimes->{{3.874683187757888*^9, 3.8746832046640673`*^9}, {
   3.874684325911029*^9, 3.87468433805895*^9}, 3.8746844273763447`*^9},
 CellLabel->"In[70]:=",ExpressionUUID->"fd0f325c-3d1d-4f03-974c-968b36b956f5"],

Cell[BoxData["62.919598789496035`"], "Output",
 CellChangeTimes->{{3.874683190290964*^9, 3.874683205544626*^9}, {
   3.87468432912158*^9, 3.874684338673973*^9}, 3.87468442749623*^9},
 CellLabel->"Out[70]=",ExpressionUUID->"972335dc-9406-48ad-b659-ea891bdd0371"]
}, Open  ]],

Cell[TextData[{
 "Counting up to the ",
 Cell[BoxData[
  FormBox[
   RowBox[{"2", "*", 
    SuperscriptBox["10", "17"], " "}], TraditionalForm]],
  FormatType->TraditionalForm,ExpressionUUID->
  "4c828977-ace7-484a-afbd-ce79379e176f"],
 "th prime should be safe."
}], "Text",
 CellChangeTimes->{{3.8746840649377117`*^9, 
  3.8746840908813677`*^9}},ExpressionUUID->"bbfc6f4d-29a2-4330-8efb-\
16d47718f918"]
},
WindowSize->{606., 659.25},
WindowMargins->{{Automatic, 378.75}, {-17.25, Automatic}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"e9616e7a-6e00-461e-9668-abe5e2eed5fd"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 569, 16, 35, "Text",ExpressionUUID->"1246511a-6110-4563-8c9d-88861b0ff3d8"],
Cell[1130, 38, 393, 11, 35, "Text",ExpressionUUID->"edae2ced-4e89-4d18-b92b-a225d0657680"],
Cell[1526, 51, 350, 11, 43, "Input",ExpressionUUID->"de8e58b6-445f-467e-859d-236c912c521d"],
Cell[1879, 64, 223, 5, 35, "Text",ExpressionUUID->"3daa7ba1-f2e2-489a-8c91-14a234ddbd91"],
Cell[CellGroupData[{
Cell[2127, 73, 768, 21, 43, "Input",ExpressionUUID->"25600010-176a-4efb-b39d-f56e0758082a"],
Cell[2898, 96, 272, 6, 36, "Output",ExpressionUUID->"4ae815a0-5bee-465c-ad8a-4bc61200daaa"]
}, Open  ]],
Cell[3185, 105, 174, 3, 35, "Text",ExpressionUUID->"26b9cecc-9a8f-45e0-8fe2-65aa2712ab89"],
Cell[3362, 110, 980, 30, 116, "Input",ExpressionUUID->"1bb6ce35-deaa-42fc-9e0c-17e2907aed10"],
Cell[CellGroupData[{
Cell[4367, 144, 942, 24, 54, "Input",ExpressionUUID->"7c991eee-99f3-4b6b-88ed-c88bd121f574"],
Cell[5312, 170, 4504, 92, 226, "Output",ExpressionUUID->"3897d396-49b9-47f1-87ff-29bf3cdaf09d"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9853, 267, 475, 12, 32, "Input",ExpressionUUID->"f7500038-837f-40e9-9d1a-aef5cf5c827b"],
Cell[10331, 281, 167, 2, 33, "Output",ExpressionUUID->"fd173feb-d248-404e-9d42-b9586a53161a"]
}, Open  ]],
Cell[10513, 286, 444, 12, 35, "Text",ExpressionUUID->"aa770861-3a59-429e-bf25-190c9a1fb76b"],
Cell[10960, 300, 167, 3, 35, "Text",ExpressionUUID->"f3da7bcb-8d53-4d92-a2de-6dc89639e16d"],
Cell[CellGroupData[{
Cell[11152, 307, 366, 8, 32, "Input",ExpressionUUID->"e70ad79a-7c8b-4ac8-9ac9-2de2702a38be"],
Cell[11521, 317, 190, 2, 33, "Output",ExpressionUUID->"88a7560a-2750-4831-9d90-d4416e4d6b9e"]
}, Open  ]],
Cell[11726, 322, 209, 4, 35, "Text",ExpressionUUID->"57f5920f-b14f-46b2-af51-eabf30ca39ca"],
Cell[CellGroupData[{
Cell[11960, 330, 472, 12, 29, "Input",ExpressionUUID->"1be41e46-90c9-427f-bd34-f21db754db79"],
Cell[12435, 344, 249, 6, 36, "Output",ExpressionUUID->"fb826548-01de-437a-adf8-7f24374215a8"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12721, 355, 415, 9, 32, "Input",ExpressionUUID->"fd0f325c-3d1d-4f03-974c-968b36b956f5"],
Cell[13139, 366, 261, 3, 56, "Output",ExpressionUUID->"972335dc-9406-48ad-b659-ea891bdd0371"]
}, Open  ]],
Cell[13415, 372, 405, 12, 36, "Text",ExpressionUUID->"bbfc6f4d-29a2-4330-8efb-16d47718f918"]
}
]
*)

(* End of internal cache information *)

